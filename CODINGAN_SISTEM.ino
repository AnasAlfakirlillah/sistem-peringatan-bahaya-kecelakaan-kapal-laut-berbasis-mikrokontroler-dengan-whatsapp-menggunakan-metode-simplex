//deklarasi header
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <NewPing.h>

//deklarasi variabel ssid dan password tipe data char
const char* ssid = "HOTSPOT";
const char* password = "Anas12345";

//deklarasi pin trig echo nodemcu
#define trigPin D8
#define echoPin D7

//jarak maksimum sensor jsn
#define MAX_DISTANCE 400

//deklarasi variabel sonar call function
NewPing sonar = NewPing(trigPin, echoPin, MAX_DISTANCE);

//objek
WiFiClient client;
HTTPClient http;

  //deklarasi variabel lastime timerdelay tipe data unsigned long
unsigned long lastTime = 0;
unsigned long timerDelay = 5000;

void setup() {
  Serial.begin(9600); //baud rate
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password); //proses koneksi ke wifi
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print("Connecting..");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  delay(50); //waktu tunda
  //  conditional
  if ((millis() - lastTime) > timerDelay) { //jika hasil variabel pengurangan lebih besar dari timer delay
    if (WiFi.status() == WL_CONNECTED) { //jika status wifi sama dengan wl connected
      if (sonar.ping_cm() > 20 && sonar.ping_cm() <= 30) { //jika sonar lebih besar dari 0 dan lebih kecil sama dengan 25
        //server api whatsapp
        String serverPath = "http://api.callmebot.com/whatsapp.php?phone=+6282256829783&text=Kelebihan+Berat+Muatan&apikey=819655";
        http.begin(client, serverPath.c_str()); //proses kirim pesan dari nodemcu ke wa
        int httpResponseCode = http.GET(); //hasil respon berupa angka
        if (httpResponseCode == 200) { //jika respon sama dengan 200
          //fungsi cetak teks
          Serial.print("Jarak = ");
          Serial.print(sonar.ping_cm());
          Serial.println("cm");
          Serial.println("Kelebihan Berat Muatan");

          Serial.print("HTTP Response code: ");
          Serial.println(httpResponseCode);
          String payload = http.getString();
          Serial.println(payload);
        } else {
          Serial.print("Error code: ");
          Serial.println(httpResponseCode);
        }
        http.end();
      }
    } else { 
      Serial.println("WiFi Disconnected");
    }
    lastTime = millis();
  }
}
